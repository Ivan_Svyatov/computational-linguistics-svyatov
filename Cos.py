# coding=utf-8
import operator
import index
from scipy import spatial, math
from collections import Counter

def compute_TFIDF(data):
    def compute_TF(text):
        text_TF = Counter(text)
        for i in text_TF:
            text_TF[i] = text_TF[i]/float(len(text_TF))
        return text_TF

    def compute_IDF(word, data):
        return math.log10(len(data) / sum([1.0 for i in data if word in i]))

    documents_list = []
    for text in data:
        TF_IDF_dictionary = {}
        computed_TF = compute_TF(text)
        for word in computed_TF:
            TF_IDF_dictionary[word] = computed_TF[word] * compute_IDF(word, data)
        documents_list.append(TF_IDF_dictionary)
    return documents_list

def nearest_docs_by_cos_dist(sentences, query):
    index = index.load_reversed_index(sentences)
    normalized_query = index.normalize_words(query.split(" "))

    docs_set = set()
    weigths_vector = [1 for i in normalized_query]

    for word in normalized_query:
        if index.get(word):
            for sent_vec in index.get(word):
                docs_set.add(sent_vec)

    doc_TFIDF_list_with_freq_map = compute_TFIDF(sentences)

    mapa = {}

    for doc in docs_set:
        doc_vector = []
        for word in normalized_query:
            if doc_TFIDF_list_with_freq_map[doc].get(word):
                doc_vector.append(doc_TFIDF_list_with_freq_map[doc].get(word))
            else:
                doc_vector.append(0)

        mapa.update({doc: doc_vector})

    cosine_distances = {}
    for doc_id, doc_vector in mapa.items():
        cosine_dist = spatial.distance.cosine(doc_vector, weigths_vector)
        cosine_distances.update({doc_id: cosine_dist})

    result=[]
    for doc in sorted(cosine_distances.items(), key=operator.itemgetter(1))[:10]:
        result.append(sentences[int(doc[0])])
    return result

def main():
    sentences = index.parse_xml("C/output.xml")

    query = "Об этом сообщил программный директор канала HBO Кейси Блойс".decode("utf-8")

    for i in nearest_docs_by_cos_dist(sentences, query):
        print (i)
