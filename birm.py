import operator

import Cos
import index
import binary_index


def main():
    sentences = index.parse_xml("C/output.xml")

    query = "Зал рассчитан на 1 тыс".decode("utf-8")

    N = 10
    fisrt_result = Cos.nearestDocsByCosDist(sentences, query)
    for result in fisrt_result:
        print (result)

    input_var = [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    R = 0
    for word_index in input_var:
        if word_index == 1:
            R += 1

    doc_vec = binary_index(fisrt_result, query)

    # print (doc_vec)

    n = []
    query_words = query.split(" ")
    word_index = 0
    for q in query_words:
        n.append(0)
        for doc in doc_vec:
            # print (doc[word_index])
            if doc[word_index] == 1:
                n[word_index] += 1
        word_index += 1

    r = []
    j = 0
    for q in query_words:
        r.append(0)
        for i in input_var:
            if i == 1:
                r[j] += doc_vec[i][j]
        j += 1

    a = [z / R for z in r]

    b = []
    r_index = 0
    for i in r:
        res = (n[r_index] - i) / (N - R)
        b.append(res)
        r_index += 1

    weigth_vector = []
    a_index = 0
    for i in a:
        res = (i + 0.5) * (1 - b[a_index] + 0.5) / (
            (b[a_index] + 0.5) * (1 - i + 0.5))
        weigth_vector.append(res)
        a_index += 1

    new_doc_weights = []
    for doc in doc_vec:
        res = 0
        weight_index = 0
        for term_weight in weigth_vector:
            res += doc[weight_index] * term_weight
            weight_index += 1
        new_doc_weights.append(res)

    print ("R=" + str(R) + "\n")
    print ("n=" + str(n) + "\n")
    print ("r=" + str(r) + "\n")
    print ("a=" + str(a) + "\n")
    print ("b=" + str(b) + "\n")
    print ("weigth_vector=" + str(weigth_vector) + "\n")
    print ("doc_vec=" + str(doc_vec) + "\n")

    map = {}
    fisrt_result_counter = 0
    for weight in new_doc_weights:
        map.update({fisrt_result[fisrt_result_counter]: weight})
        fisrt_result_counter += 1

    final_result = []
    for doc in sorted(map.items(), key=operator.itemgetter(1))[:10]:
        final_result = map.keys()
    for result in final_result:
        print (result)
    return map;
