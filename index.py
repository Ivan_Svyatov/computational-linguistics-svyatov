# coding=utf-8
import os
import re
import io
import pymorphy2
from nltk.corpus import stopwords
from lxml import etree


def parse_xml(file):
    text = []
    tree = etree.parse(file)
    doc = tree.getroot()
    for node in doc:
        text.append(node.text)
    return text


def index(text):
    i = 0
    x = {}
    for sent in text:
        words = sent.split(" ")
        stop_words = stopwords.words('russian')
        words = [i for i in words if (i not in stop_words and i != " ")]
        words = normalize(words)
        update_index(x, words, i)
        i += 1
    return x


def normalize(words):
    morph = pymorphy2.MorphAnalyzer()
    words_to_ret = list(map(lambda word: morph.parse(word)[0].normal_form, words))
    return words_to_ret


def update_index(x, words, i):
    for word in words:
        if x.get(word):
            x.get(word).append(i)
        else:
            x.update({word: [i]})

text = parse_xml("C\output.xml")
reversed_index = index(text)

text = parse_xml("S\output.xml")
reversed_index = index(text)