# coding=utf-8
from lxml import etree
import os
import re
import io


def to_xml(page, f):
    sentence = ""
    while True:
        buf = f.read(1)
        if not buf: break
        if buf == "\n" or buf == "?" or buf == "!" or buf == ".":
            if sentence == "":
                continue
            current_tag_xml = etree.SubElement(page, 'sent')
            sentence = re.sub(r"[$«»()\.,%/\-—–:;\n]", '', sentence)
            print (sentence)
            current_tag_xml.text = sentence
            sentence = ""
        else:
            sentence += buf

    if sentence != "":
        current_tag_xml = etree.SubElement(page, 'sent')
        sentence = re.sub(r"\W", '', sentence)
        current_tag_xml.text = sentence
    return


def to_xml_file(dir, files):
    page = etree.Element('document')
    for file in files:
        print(file)
        file = io.open(dir + file, encoding='windows-1251')
        to_xml(page, file)
        output = io.open(dir + 'output.xml', 'w', encoding='utf-8')
        output.write(etree.tounicode(page, pretty_print=True))


dir = 'S/'
if os.path.isfile(dir + 'output.xml'):
    os.remove(dir + 'output.xml')
files = os.listdir(dir)
to_xml_file(dir, files)

dir = 'C/'
if os.path.isfile(dir + 'output.xml'):
    os.remove(dir + 'output.xml')
files = os.listdir(dir)
to_xml_file(dir, files)