def binary_index(text, query):
    doc_vector = []
    query_words = query.split(" ")
    for t in text:
        t_vec = []
        words = t.split(" ")
        for q in query_words:
            fl = 0
            for word in words:
                if word == q:
                    fl = 1
                    break;
            t_vec.append(fl)
        doc_vector.append(t_vec)
    return doc_vector